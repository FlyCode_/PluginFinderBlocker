package cc.flycode.PluginsBlock.Blocker;

import cc.flycode.PluginsBlock.PluginFinderBlocker;
import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.events.PacketAdapter;
import com.comphenix.protocol.events.PacketContainer;
import com.comphenix.protocol.events.PacketEvent;

/**
 * Created by FlyCode on 21/07/2018 Package cc.flycode.PluginsBlock.Blocker
 */
public class Blocker {
    public void init() {
        ProtocolLibrary.getProtocolManager().addPacketListener(new PacketAdapter(PluginFinderBlocker.instance, PacketType.Play.Client.TAB_COMPLETE) {
            public void onPacketReceiving(final PacketEvent event) {
                PacketContainer packet = event.getPacket();
                String message = packet.getStrings().read(0);
                String cmd = message.toLowerCase();
                if (!event.getPlayer().isOp() && !event.getPlayer().hasPermission("tabblocker.bypass") && cmd.startsWith("/")) {
                    event.setCancelled(true);
                }
            }
        });
    }
}
