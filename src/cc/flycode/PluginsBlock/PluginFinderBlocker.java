package cc.flycode.PluginsBlock;

import cc.flycode.PluginsBlock.Blocker.Blocker;
import org.bukkit.event.Listener;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Created by FlyCode on 21/07/2018 Package cc.flycode.PluginsBlock
 */
public class PluginFinderBlocker extends JavaPlugin implements Listener {
    public static Plugin instance;
    @Override
    public void onEnable() {
        instance = this;
        Blocker blocker = new Blocker();
        blocker.init();
    }
}
